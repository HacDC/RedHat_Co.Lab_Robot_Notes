#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Written by Kevin Cole <ubuntourist@hacdc.org> 2021.03.23
#
# Read and echo characters and send to micro:bit
#
# Upload microbit_recv.py to the micro:bit then start this program.
#
# See:
#     https://pyserial.readthedocs.io/en/latest/shortintro.html
#

import serial
import sys
from getch import *

microbit = serial.Serial("/dev/ttyACM0")

CTRL_C, CTRL_D = chr(3), chr(4)

c = None
while c not in (CTRL_C, CTRL_D):
    c = getch()
    print(c, end="", flush=True)
    microbit.write(c.encode())
sys.exit()
