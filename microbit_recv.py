"""
Written by Kevin Cole <ubuntourist@hacdc.org> 2021.03.23
Start microbit_xmit.py on your computer
at the other end of the USB cable...
"""
serial.redirect_to_usb()

def on_forever():
    ping = serial.read_buffer(0)
    if len(ping) > 0:
        basic.show_string("" + (ping.to_string()))
basic.forever(on_forever)
