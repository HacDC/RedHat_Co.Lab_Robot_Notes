# Sparkfun Red Hat Co.Lab Robot
# (BBC micro:bit + Sparkfun moto:bit)

This material is being developed in conjunction with
[HacDC's](https://hacdc.org/) 2021 [Robot
Introduction](https://wiki.hacdc.org/index.php?title=Robot_Project).

----

## <a id="toc"></a>Table of Contents

* [Sparkfun moto:bit Carrier Board](#carrier)
* [Wheel Motor Assembly](#wheel)
* [Line folowing sensors](#sensors)
* [Chassis](#chassis)
* [Wiring](#wiring)
* [Powering on](#booting)
* [Uploading code to the micro:bit](#uploading)
* [Disconnect from the web](#stand_alone)
* [REPL achieved!](#REPL)
* [micro:bit pin-outs for the moto:bit](#pin_outs)
* [WARNING! WARNING! DANGER, WILL ROBINSON! Assembly Language!](#assembly)
* [micro:bit v. 2.0](#microbit.v2)
* [Flasining MicroPython v. 2.0 firmware](#micropython.v2)
* [Error code 529](#error_code_529)
* [Mu and Thonny](#mu_and_thonny)
* [Speak to me!](#speech)
* [MakeCode demos](#makecode)
* [Audio](#audio)
* [MicroPython for v.2.0 Still in BETA!](#beta)
* [Official reference documentation](#documentation)
* [Hardware deep dive](#hardware)
* [Future hardware purchases?](#future)
* [Bluetooth](#bluetooth)

----

## <a id="carrier"></a>[Sparkfun moto:bit Carrier Board](#toc)

The [moto:bit](https://www.sparkfun.com/products/15713) - **micro:bit**
Carrier Board (Qwiic) uses a Qwiic interface? and offers pins for:

* six 3-pin sensors,
* two 3-pin servo motors
* two 2-pin "big" motors

and an I2C bus (as well as a Qwiic "port" and power switch)

```
       +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                              micro:bit CONNECTOR BUS


           ANALOG           DIGITAL
           DIGITAL            ONLY
              |                |
       +-+----+-------+ +------+------+-+
       | |            | |             | |                           SCL  ☐
    P0 • •  P1     P2 • • P8      P12 • • P14      P15 • • P16      SDA  ☐
   3V3 • • 3V3    3V3 • • 3V3     3V3 • • 3V3      VCC • • VCC      3V3  ☐
   GND • • GND    GND • • GND     GND • • GND      GND • • GND      GND  ☐

      SENSOR         SENSOR          SENSOR           SERVO         I2C BUS



        BLACK •   LEFT                           RIGHT  • RED
        RED   •   MOTOR                          MOTOR  • BLACK


   POWER IN
  INPUT RANGE
  VCC: 3 - 11V                                                  QWIIC


                  STOP                RUN
                 MOTORS              MOTORS
```

----

## <a id="wheel"></a>[Wheel Motor Assembly](#toc)

* Geez! They weren't kidding! Getting the front motor mounts over the
  big motors was a PITA! Ultimately, brute forcing it with my hand
  rather than pliers proved the most successful.

* The rear motor mounts were only slightly better.

* Mounting the assemblage to the bottom chassis, I was sure I was
  going to snap something in two or tear out a wire.

* The motor I've mounted on the right side has a black wire whose
  solder point looks suspect...

----

## <a id="sensors"></a>[Line folowing sensors](#toc)

* The pins should be sticking out on the smooth side of the "bottom"
  mount.

* By contrast, this slipped together so easily that I worry it might
  be too loose, though it will undoubtedly tighten up when mounted
  to the chassis.

* This tutorial seems to be a thinly disguised advertisment for
  electrical tape...

----

## <a id="chassis"></a>[Chassis](#toc)

* Ages 10 and up, my ass. This required a hell of a lot of strength to
  snap the chassis together without snapping.

* Don't forget: there are **eight** snap points: Four at the corners
  and two above the wheels. I got the outer four, and didn't notice
  the wheel ones until after the fact. It took forever to get those
  to snap into place.

----

## <a id="wiring"></a>[Wiring](#toc)

* Motor wires _may_ need to be flipped (black to red and vice versa).

* **IMPORTANT SAFETY TIP**: The instructions fail to mention that the
  battery pack should be rotated 90 degrees after slipping in the back
  so that the power cable is coming out the left side behind the wheel
  -- or if the instructions did mention it, that tidbit wasn't clear
  to me.  After assembling, I found a guide to assembling the chassis
  without the **moto:bit** or **micro:bit**, and it has an entire
  section specifically devoted to [installing the battery
  pack](https://learn.sparkfun.com/tutorials/assembly-guide-for-redbot-with-shadow-chassis/9-batteries)


* God help you if you ever have to replace the batteries...

----

## <a id="booting"></a>[Powering on](#toc)

* Providing power causes the **micro:bit** to boot with an
  introductory "H...E...L...L...O" and then prompts you to push
  buttons **A** and then **B**.

----

## <a id="uploading"></a>[Uploading code to the micro:bit](#toc)

* Connecting the **micro:bit** to desktop or laptop computer via USB
  provides sufficient power to program it. No need to waste the
  "eternal life" batteries, while downloading code to the
  **micro:bit**. So I would recommend keeping the **moto:bit**
  disconnected from the battery pack.

* The short MicroUSB to USB cable works better for me if I disconnect
  the **micro:bit** from the **moto:bit** and flip it upside down,
  exposing the side with all the writing, the **RESET** button, and
  the yellow LED.

* [Identify your micro:bit
  version](https://support.microbit.org/support/solutions/articles/19000119162-how-to-identify-the-version-number-of-your-micro-bit-).
  There is a bit of contradictory information on the page, but it
  turns out, my kit contained a micro:bit **v 1.5** but there's a
  newer **v 2.0**. Disappointing... One would have expected SparkFun
  to send the latest and greatest.

* **REALLY** disappointing! The v 2.0 has a built-in microphone and
  speaker. The v 1.5 does not! That kind of blows my whole reason for
  playing with this! And it's out of stock on SparkFun and AdaFruit.
  I'm back-ordering it on SparkFun.

* Speaking of latest and greatest, just to be sure, I like to know I'm
  using the most recent version of the
  [firmware](https://microbit.org/get-started/user-guide/firmware/).

* After "upgrading" (though I think it already had the latest) my
  `DETAILS.TXT` contains:

```
    # DAPLink Firmware - see https://mbed.com/daplink
    Unique ID: 9901000052374e450046100f000000100000000097969901
    HIC ID: 97969901
    Auto Reset: 1
    Automation allowed: 0
    Overflow detection: 0
    Daplink Mode: Interface
    Interface Version: 0249
    Bootloader Version: 0243
    Git SHA: 9c5fd81e6545d00b7f7c21ca9d8577dbd6a5fed2
    Local Mods: 0
    USB Interfaces: MSD, CDC, HID, WebUSB
    Bootloader CRC: 0x32eb3cfd
    Interface CRC: 0xcdb7b2a3
    Remount count: 0
    URL: https://microbit.org/device/?id=9901&v=0249
```

* The **micro:bit** is supposed to pair via the web using
  Chromium. I'm using "**Chromium Version 87.0.4280.141 (Developer
  Build) built on Debian 10.7, running on Debian bullseye/sid
  (64-bit)**" according to Chromium's ["About"
  page](chrome://settings/help).

* The [WebUSB troubleshooting
  guide](https://support.microbit.org/support/solutions/articles/19000105428-webusb-troubleshooting)
  offers the Linux fix to pairing. At the command line:

```
    $ getent group plugdev >/dev/null || sudo groupadd -r plugdev
    $ sudo cat > /etc/udev/rules.d/50-microbit.rules
    SUBSYSTEM=="usb", ATTR{idVendor}=="0d28", MODE="0664", GROUP="plugdev"
    ^C
    $ sudo usermod -a -G plugdev «your username»
    $ sudo udevadm control --reload-rules
    $ sudo shutdown -r now
```

* After pairing? Or perhaps uploading? there is a new file out on the
  **micro:bit**: `ASSERT.TXT` which contains:

```
    Assert
    File: ..\..\..\source\daplink\drag-n-drop\file_stream.c
    Line: 165
    Source: Application
```

* The uploaded [.hex](https://en.wikipedia.org/wiki/Intel_HEX) file
  appears in the directory momentarily but then is consumed by the
  microprocessor and disappears.

* The backslashes seem suspiciously Microsoft and I'm wondering what
  three levels up from "root" is being represented by `..\..\..\`

* The Micro:bit Educational Foundation has a [YouTube
  channel](https://www.youtube.com/channel/UCJRGTnzeb0esPmsE-mFkPGg/about)
  with educational videos.

----

## <a id="stand_alone"></a>[Disconnect from the web](#toc)

I want to write Python code, and load it into the beastie without need
of a network. I have a computer, I have a USB cable and I have a
**micro:bit** and a **moto:bit**. I should not need the internet in
order to make use of that. On Ubuntu 20.04, the following works:

```
    $ apt install micropython    micropython-doc \
                  python3-uflash python3-uflash-doc
    ...
    The following additional packages will be installed:
      firmware-microbit-micropython python3-nudatus
    Suggested packages:
      firmware-microbit-micropython-doc python3-doc
      firmware-microbit-micropython-dl wwww-browser
    The following NEW packages will be installed:
      firmware-microbit-micropython micropython micropython-doc
      python3-nudatus python3-uflash python3-uflash-doc
    0 upgraded, 6 newly installed, 0 to remove and 0 not upgraded.
    Need to get 1,557 kB of archives.
    After this operation, 6,232 kB of additional disk space will be used.

    $ apt install firmware-microbit-micropython-doc
    The following NEW packages will be installed:
      firmware-microbit-micropython-doc
    0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
    Need to get 10.9 MB of archives.
    After this operation, 16.6 MB of additional disk space will be used.
```

* The `uflash` is needed to flash the code onto the **micro:bit**,
  apparently.

* [MicroPython on the
  micro:bit](https://tech.microbit.org/software/micropython/)
* a simple [moto:bit driver](https://github.com/hsshss/motobit-micropython)

----

## <a id="REPL"></a>[REPL achieved!](#toc)

It probably completely messes up the Microsoft MakeCode flow
completely, but I now have the MicroPython REPL prompt:

```
    $ cp -v /usr/share/firmware-microbit-micropython/firmware.hex \
            /media/kjcole/MICROBIT/
    $ screen /dev/ttyACM0
```

This appeared to work, in that the `cp` command flashed the yellow LED
and after starting `screen`, I could see that typing was causing the
yellow LED to flash, but it wasn't echoing or, for that matter,
responding to carefully typed Python statements. In an attempt to
correct...

```
    $ apt install firmware-microbit-micropython-dl
    ...this REPLACES firmware-microbit-micropython !!!
    $ uflash
    $ minicom -D /dev/ttyACM0
    Welcome to minicom 2.7.1

    OPTIONS: I18n
    Compiled on Dec 23 2019, 02:06:26.
    Port /dev/ttyACM0, 16:20:36

    Press Meta-Z for help on special keys

    >>>
```

Was it the fact that I used a freshly downloaded firmware as a result
pf switching to `firmware-microbit-micropython-dl` or was it the fact
that I used `uflash` instead of `cp ...` that made the whole thing
work? Or perhaps that replacing `screen` with `minicom`...

(By the way, further investigation showed `minicom` reporting a baud
rate of 115200.)

* `microfs` the **micro:bit** file system tool, looks useful. So,
   following the [microfs guide](https://microfs.readthedocs.io/en/latest/),
   I've installed it as a local Python package via

```
    $ pip install --user microfs
    $ ufs ls
    $ ufs put README.md
    $ ufs ls
    README.md
    $ ufs rm README.md
    $ ufs ls
    $
```

As for writing in MicroPython, see the [BBC micro:bit MicroPython
documentation](https://microbit-micropython.readthedocs.io/en/latest/).
And, as always, start with "Hello World":

```
    $ cat > hello.py
    #!/usr/bin/env micropython
    # -*- coding: utf-8 -*-
    #
    # Hello World 2021.02.09
    #

    from microbit import *

    display.scroll("Hello World")
    ^C
    $ uflash hello.py
```

Success! "Hello World" scrolls across the LEDs.

**Caveat:** Each time `uflash` is run, it dismounts the **micro:bit**,
which then has to be re-mounted for any subsequent flashing. (I've
just been using the `dolphin` file browser to do that.)

See also:

* [MicroPython documentation](http://docs.micropython.org/en/latest/)
* [micro:bit MicroPython guide](https://microbit.org/get-started/user-guide/python/)
* [Python Editor for micro:bit](https://python.microbit.org/v/2)
  (through the web)

----

## <a id="pin_outs"></a>[micro:bit pin-outs for the moto:bit](#toc)

Start with a [micro:bit
overview](https://microbit.org/get-started/user-guide/overview/).

In order to use MicroPython directly with the **moto:bit** carrier board,
one needs the [pin-out
mapping](https://microbit.pinout.xyz/sparkfun-motobit.html) betwixt
the **micro:bit** and the **moto:bit**.

The lazy can use a [moto:bit Python
module](https://github.com/hsshss/motobit-micropython) found on
GitHub. It's a pretty tiny bit of Python doggerel, easily digested,
and worthy of direct study.

See also:

* [pxt-moto-bit](https://makecode.microbit.org/pkg/sparkfun/pxt-moto-bit)
  documentation
* [motobit.ts](https://github.com/sparkfun/pxt-moto-bit/blob/master/motobit.ts)
  in the `pxt-moto-bit` source git repository on GitHub

----

## <a id="assembly"></a>[WARNING! WARNING! DANGER, WILL ROBINSON! Assembly Language!](#toc)

Here be dragons! I'm getting **WAY** ahead of myself here, but I think
eventually, I might just skip directly to assembly language due to
recent experiences with other architectures.

* According to Wikipedia, the **micro:bit** uses an [ARM
  Cortex-M0](https://en.wikipedia.org/wiki/ARM_Cortex-M#Cortex-M0) for
  it's main brain.

* [Programming of ARM Cortex-M
  microcontrollers](https://svenssonjoel.github.io/pages-2021/cortex-m-assembler-0/)
  appears to be a reasonable place to start the journey down this
  particular rabbit hole.

----

## <a id="microbit.v2"></a>[micro:bit v. 2.0](#toc)

I'm not sure what I was expecting from such a tiny board and its tiny
speaker... It works, but it definitely is wee sound. The `DETAILS.TXT`
that came with the new beastie reads as follows:

```
    # DAPLink Firmware - see https://mbed.com/daplink
    Unique ID: 9904360259994e450012400500000019000000009796990b
    HIC ID: 9796990b
    Auto Reset: 1
    Automation allowed: 0
    Overflow detection: 0
    Incompatible image detection: 1
    Page erasing: 0
    Daplink Mode: Interface
    Interface Version: 0255
    Bootloader Version: 0255
    Git SHA: 1436bdcc67029fdfc0ff03b73e12045bb6a9f272
    Local Mods: 0
    USB Interfaces: MSD, CDC, HID, WebUSB
    Bootloader CRC: 0x828c6069
    Interface CRC: 0x5b5cc0f5
    Remount count: 0
    URL: https://microbit.org/device/?id=9904&v=0255
```

----

## <a id="micropython.v2"></a>[Flasining MicroPython v. 2.0 firmware](#toc)

And... crap. Attempting to upgrade the 2.0 with MicroPython, using the
steps above, resulted in ":frowning: 529" in the LED grid and the
creation of a `FAIL.TXT` file containing the text:

```
    error: The application image is not compatible with the target.
    type: user
```

## <a id="error_code_529"></a>[Error code 529](#toc)

micro:bit support offers up the page: [micro:bit Error
codes](https://support.microbit.org/support/solutions/articles/19000016969-micro-bit-error-codes)
which, regarding  ":frowning: 529", states:

> The micro:bit has detected an incompatible image. This can occur
> when you If you attempt to use an old .hex file with the latest
> micro:bit. Try updating the hex file in the editor in which it was
> created and downloading it again.

Looking at `firmware-microbit-micropython-dl` indicated... not much.
Tracing backwards to where `uflash` comes from, there is an indication
that a `py2hex` utility is included with `uflash`. (As you might
suspect from the name, `py2hex` reads a `.py` file and produces a
`.hex` file.) Not on my system. So. Remove the system-wide Debian
package and add a user-space Python package instead:

```
    $ apt purge python3-uflash
    ...
    The following packages will be REMOVED:
      python3-uflash*
    0 upgraded, 0 newly installed, 1 to remove and 0 not upgraded.
    After this operation, 62.5 kB disk space will be freed.

    $ pip install --user uflash
```

Now I have `py2hex`.

And, it turns out there is a solution to the firmware issue too.

```
    $ git clone https://github.com/microbit-foundation/micropython-microbit-v2
    $ cd micropython-microbit-v2
    $ git submodule update --init
    $ make -C lib/micropython/mpy-cross
    $ cd src
    $ apt install gcc-arm-none-eabi
    The following additional packages will be installed:
      binutils-arm-none-eabi libnewlib-arm-none-eabi libnewlib-dev
      libstdc++-arm-none-eabi-newlib
    Suggested packages:
      libnewlib-doc
    The following NEW packages will be installed:
      binutils-arm-none-eabi gcc-arm-none-eabi libnewlib-arm-none-eabi
      libnewlib-dev libstdc++-arm-none-eabi-newlib
    0 upgraded, 5 newly installed, 0 to remove and 0 not upgraded.
    Need to get 271 MB of archives.
    After this operation, 2,340 MB of additional disk space will be used.
    $ make
```

This produced `MICROBIT.hex` in the `src/` directory, however,
`uflash` and `ufs put` both complained. A straight `cp` did the trick.

```
    $ minicom -D /dev/ttyACM0
    Welcome to minicom 2.7.1

    OPTIONS: I18n
    Compiled on Dec 23 2019, 02:06:26.
    Port /dev/ttyACM0, 14:33:55

    Press Meta-Z for help on special keys

    F52833
    Type "help()" for more information.
    >>>
```

As of 2021.02.12, MicroPython for the micro:bit 2.0 has 21 [open
issues](https://github.com/microbit-foundation/micropython-microbit-v2/issues),
some of which are audio-related...

----

## <a id="mu_and_thonny"></a>[Mu and Thonny](#toc)


### Mu

The [Mu Editor](https://codewith.mu/) and [Thonny](https://thonny.org/)
are two editors that know about both **MicroPython** and the **BBC
micro:bit**.

But, at least on Ubuntu 20.04 and reportedly Linux Mint, **Mu** has
become a bit out of date and attempts to install newer versions
"normally" fail. One can clone the git repository from
[GitHub](https://github.com/mu-editor/mu) and built in a virtual
environment, but that's probably beyond most.

### Notes to myself only...

After fighting with the cloned Git repository, plus `pipenv`...
```
$ cd ~/gits/HacDC/microprocessors/mu
$ pipenv shell
./run.py
```

now appears to work

### Thonny

**Thonny** is a slightly less friendly Python editor but is still
quite friendly.

If you burned the MicroPython firmware into the micro:bit, a pseudo
file system is created, and your source code is no longer converted
into a hex file that "disappears". Instead, the Python source code
is hidden somewhere that Thonny can see but the Linux OS cannot.

**IMPORTANT SAFETY TIP:** If you want to have the code run on reboot,
save the file as `main.py`. The filename is important.

----

## <a id="speech"></a>[Speak to me!](#toc)

It's DAMNED soft, but yea, verily, with my ear pressed up against it,
the following didst speaketh the numbers!

```
    from microbit import *
    import speech
    for num in range(10):
        display.scroll(num)
        speech.say(str(num))
```

----

## <a id="makecode"></a>[MakeCode demos](#toc)

* [Introduction to the Musical Scale](https://github.com/kjcole/microbit-musical-scales-intro/blob/main/README.md)
* [The Twelfth Root of Two](https://github.com/kjcole/microbit-twelfth-root/blob/main/README.md)
* [Glissando](https://github.com/kjcole/microbit-glissando/blob/main/README.md)
* [Pseudo-Theremin](https://github.com/kjcole/microbit-ir-theremin/blob/main/README.md)
* [Computer as Piano Keyboard](https://github.com/kjcole/microbit-keyboarding/blob/main/README.md)

## <a id="audio"></a>[Audio](#toc)

The `audio` module "works" but the sample code in the [audio module
documentation](https://microbit-micropython.readthedocs.io/en/latest/audio.html)
gives less than satisfactory results on the micro:bit. I'm beginning
to feel that it's just too damned small to be of use.

----

## <a id="beta"></a>[MicroPython for v.2.0 Still in BETA!](#toc)

As of 2021.02.19 (six days ago as of this writing), the most recent
commit to MicroPython for the micro:bit is:

[codal_port: Bump micro:bit version to
v2.0.0-beta.4](https://github.com/microbit-foundation/micropython-microbit-v2/commits/master)

This is explaining a lot about the problems I've been having. (The
v. 1.5 uses something called DAL whereas the v. 2.0 uses CODAL. The
hex file formats are different.)

----

## <a id="documentation"></a>[Official reference documentation](#toc)

* The [reference
  documentation](https://makecode.microbit.org/reference/) as well as
  other helpful material can be found on the [official documentation
  pages](https://makecode.microbit.org/docs).

----

## <a id="hardware"></a>[Hardware deep dive](#toc)

Further documentation that is often beyond my ken (and my barbie):

* [micro:bit technical documentation: Hardware](https://tech.microbit.org/hardware/)
* [micro:bit V.2.0 schematics](https://github.com/microbit-foundation/microbit-v2-hardware/blob/main/V2/MicroBit_V2.0.0_S_schematic.PDF) (PDF)
* [SparkFun moto:bit schematics](https://cdn.sparkfun.com/assets/9/2/5/3/0/SparkFun_Micro_Bit_Moto_Bit_v20.pdf) (PDF)

----

## <a id="future"></a>[Future hardware purchases?](#toc)

* [Wikipedia: I2C](https://en.wikipedia.org/wiki/I%C2%B2C)
* [Sparkfun I2C Digital-to-Analog Converter (DAC) breakout](https://www.sparkfun.com/products/12918)
* [Adafruit I2C Stereo Audio Amplifier](https://www.adafruit.com/product/1712)
* [Sparkfun OpAmp breakout](https://www.sparkfun.com/products/9816)

----

## <a id="bluetooth"></a>[Bluetooth](#toc)

Information gleaned from [RPi - micro:bit
Workshop](https://ukbaz.github.io/howto/ubit_workshop.html) -- among
other places.

Start by getting information about your computer's Bluetooth
interface. Crank up `bluetoothctl` and issue a `show` command.
(Presumably, this will give very different information for each
computer. So, I have redacted a lot of info below, replacing with hex
with `^^` for the local machine and `vv` for the micro:bit.
"?".)

```
$ bluetoothctl
Agent registered
[bluetooth]# show
Controller ^^:^^:^^:^^:^^:^^ (public)
	Name: hostname
	Alias: hostname
	Class: 0x????????
	Powered: yes
	Discoverable: yes
	DiscoverableTimeout: 0x00000000
	Pairable: yes
	UUID: Message Notification Se.. (????????-????-????-????-????????????)
	UUID: A/V Remote Control        (????????-????-????-????-????????????)
	UUID: OBEX Object Push          (????????-????-????-????-????????????)
	UUID: Message Access Server     (????????-????-????-????-????????????)
	UUID: PnP Information           (????????-????-????-????-????????????)
	UUID: IrMC Sync                 (????????-????-????-????-????????????)
	UUID: Vendor specific           (????????-????-????-????-????????????)
	UUID: Headset AG                (????????-????-????-????-????????????)
	UUID: A/V Remote Control Target (????????-????-????-????-????????????)
	UUID: Generic Attribute Profile (????????-????-????-????-????????????)
	UUID: Phonebook Access Server   (????????-????-????-????-????????????)
	UUID: Audio Sink                (????????-????-????-????-????????????)
	UUID: Generic Access Profile    (????????-????-????-????-????????????)
	UUID: Audio Source              (????????-????-????-????-????????????)
	UUID: OBEX File Transfer        (????????-????-????-????-????????????)
	UUID: Handsfree                 (????????-????-????-????-????????????)
	Modalias: usb:???????????????
	Discovering: no
Advertising Features:
	ActiveInstances: 0x00
	SupportedInstances: 0x05
	SupportedIncludes: tx-power
	SupportedIncludes: appearance
	SupportedIncludes: local-name
	SupportedSecondaryChannels: 1M
	SupportedSecondaryChannels: 2M
	SupportedSecondaryChannels: Coded
[bluetooth]# scan on
```

Whoa! A gazillion devices are being picked up! And as far as I know, I
have no Bluetooth devices other than the BBC micro:bit and the laptop
that I just added a dongle to.

```
[bluetooth]# scan off
[bluetooth]# quit

$ bluetoothctl scan on | grep -i bbc
[DEL] Device vv:vv:vv:vv:vv:vv BBC micro:bit [?????]
[NEW] Device vv:vv:vv:vv:vv:vv BBC micro:bit [?????]
```

(While running the scan above I did the micro:bit 3-fingered salute:
Hold buttons A and B while bouncing the reset button.)

No joy:

```
$ bluetoothctl pair vv:vv:vv:vv:vv:vv
Attempting to pair with vv:vv:vv:vv:vv:vv
[CHG] Device vv:vv:vv:vv:vv:vv Connected: yes
[CHG] Device vv:vv:vv:vv:vv:vv Connected: no
Failed to pair: org.bluez.Error.AuthenticationCanceled

$ bluetoothctl pair vv:vv:vv:vv:vv:vv
Attempting to pair with vv:vv:vv:vv:vv:vv
Failed to pair: org.bluez.Error.AuthenticationCanceled
```

SUCCESS! 

To read strings from the microbit use `data = ubit._uart_tx.value` in the
Python code running on the laptop. It returns udev (?) bytes, which cannot
be printed directly, but `"".join([x for x in data])` will get it, IIRC.

----

