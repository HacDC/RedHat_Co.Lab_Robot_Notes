//  Twelfth Root of Two
let frequency = 440
//  2.0 ** (1.0 / 12.0)
let twelfth_root = 1.0594630943592953
for (let index = 0; index < 12; index++) {
    basic.pause(200)
    music.playTone(frequency, music.beat(BeatFraction.Whole))
    frequency = frequency * twelfth_root
    console.log(frequency)
}
