#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Hello World
# Copied by Kevin Cole <ubuntourist@hacdc.org> 2021.04.01
#
# See:
#
# https://microbit-micropython.readthedocs.io/en/latest/tutorials/hello.html
# https://microbit-micropython.readthedocs.io/en/latest/tutorials/speech.html
# https://www.c-sharpcorner.com/article/make-your-bbc-micro-bit-talk-using-micropython/

from microbit import *
import speech

display.scroll("Hello World")
sleep(2000)
display.show(Image.HAPPY)
display.scroll("Hello,I AM MICROBIT AND I CAN TALK")
speech.say("Hello,I AM MICROBIT AND I CAN TALK")
sleep(2000)
speech.pronounce("AEAE/HAEMM", pitch=200, speed=100)  # Ahem
sleep(1000)

# Singing requires a phoneme with an annotated pitch for each syllable.
#
solfa = ["#115DOWWWWWW",   # Doh
         "#103REYYYYYY",   # Re
         "#94MIYYYYYY",    # Mi
         "#88FAOAOAOAOR",  # Fa
         "#78SOHWWWWW",    # Soh
         "#70LAOAOAOAOR",  # La
         "#62TIYYYYYY",    # Ti
         "#58DOWWWWWW",]   # Doh

# Sing the scale ascending in pitch.
#
song = "".join(solfa)
speech.sing(song, speed=100)

# Reverse the list of syllables.
#
solfa.reverse()

# Sing the scale descending in pitch.
#
song = "".join(solfa)
speech.sing(song, speed=100)
