# Red Hat Co.Lab Robot Parts List

This material is being developed in conjunction with
[HacDC's](https://hacdc.org/) 2021 [Robot
Introduction](https://wiki.hacdc.org/index.php?title=Robot_Project).

----

* [Shadow Chassis and breakouts](#chassis)
* [Chassis parts bag](#parts)
* [Servo motor bags (2)](#motors)
* [BBC micro:bit Go Bundle](#micro_bit)
* [Other](#other)

----

## <a id="chassis"></a>[Shadow Chassis and breakouts](https://www.sparkfun.com/products/13301)

* 1 top chassis cover
* 1 bottom chassis cover
* 2 front motor mounts
* 2 encouder mounts
* 1 line follower mount
* 1 line follower mount plate

## <a id="parts"></a>Chassis parts bag

* 4 side struts
* 2 rear motor mounts
* 2 moto:bit mounts
* 1 battery pack clip

## <a id="motors"></a>Servo motor bags (2)

* 2 [servo motors plus white "gears"](https://www.sparkfun.com/products/9065) (2 bags)

## <a id="micro_bit"></a>[BBC micro:bit Go Bundle](https://www.sparkfun.com/products/14336)

* 1 BBC micro:bit v 1.5 microprocessor (v 2.0 would be better...)
* 1 USB to mini-USB cable (3" maybe?)
* 2 batteries (AAA 1.5v)
* 1 [battery holder for AAA batteries](https://www.sparkfun.com/products/15101)

## <a id="other"></a>Other

* 4 [batteries (AA 1.5v)](https://www.sparkfun.com/products/15201)
* 1 battery holder for AA batteries
* 2 [Hobby Gearmotor - 140 RPM](https://www.sparkfun.com/products/13302)
* 2 [wheels](https://www.sparkfun.com/products/13259)
* 1 nub caster
* 3 [line sensors](https://www.sparkfun.com/products/11769)
* 3 [jumper cables - 0.1", 3-pin, 6"](https://www.sparkfun.com/products/10368) (red, orange, yellow)
* 1 [moto:bit](https://www.sparkfun.com/products/15713) (motor contoller / power supply)
