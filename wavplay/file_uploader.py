#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Cabe Atwell Microbit WAV Sound File uploader  6/2019
#

import os
import sys
from os.path import expanduser
from microfs import ls, rm, put, get, get_serial

# wav="drum beat 0.wav"
raw = "output.raw"  # output file name

# check if command line is correct
if len(sys.argv) != 2:
    print("Error in command line.")
    if len(sys.argv) < 2:
        print("No filename specified.")
    else:
        print("Too many arguments.")
    print("Usage: python3 file_uploader.py [source_wav_file_name]")
    exit()
else:
    wav = expanduser(sys.argv[1])  # WAV file to be processed

# convert wav to mono 8 bit
ffmpeg = f'ffmpeg -y -i "{wav}" -acodec pcm_u8 -f u8 -ac 1 -ar 7812 "{raw}"'
os.system(ffmpeg)

try:
    microbits_list = get_serial()  # check if micro:bit board is connected
except:
    try:
        microbits_list
    except NameError:
        microbits_list = []

    if len(microbits_list) == 0:
        print("No micro:bit board connected. Exiting.")

else:
    Microbit_fileList = ls(serial=None)
    # if files present - print file names, erase files
    if Microbit_fileList:
        print("Files on the micro:bit board at the moment:")
        for filename in Microbit_fileList:
            print(f"\t{ filename}")

        print("Erasing files...")
        for filename in Microbit_fileList:
            print(f"\t{filename}")
            rm(filename, serial=None)
    else:
        print("No files on the micro:bit board at the moment.")

    # upload converted file to the micro:bit
    try:
        print(f"Uploading {raw}...")
        put(raw, target=None, serial=None)
    except IOError:
        print("Upload failed")
    else:
        print("Upload complete")
