# Cabe Atwell Microbit WAV Sound File Player code 6/2019
from microbit import *
import audio


def read_frame(f_list, frame):
    for file in f_list:                # could play several files
        ln = file.readinto(frame)      # reading data, ln - actually read
        while ln:                      # while read OK
            yield frame                # return frame
            ln = file.readinto(frame)  # read another one after return


def play_file(f):
    with open(f, "rb") as file1:  # opening file
        f_list = [file1]          # we have a single file
        # calling "audio.play" with "read_frame" as a
        # callback to provide samples from file, wait
        audio.play(read_frame(f_list, frame), wait=True)


# Allocate memory outside the interrupt
frame = audio.AudioFrame()
ln    = -1                       # init -1 as "not read OK"
file  =  1

while 1:                         # play the files
    if button_a.is_pressed():
        play_file("output.raw")
