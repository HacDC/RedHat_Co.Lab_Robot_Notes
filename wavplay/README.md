# Element 14 micro:bit WAV player

Cabe Atwell provided a blog entry with a ZIP file: [Project: BBC
micro:bit WAV Sound File Player by Cabe
(2019.06.10)](https://www.element14.com/community/community/stem-academy/microbit/blog/2019/06/10/project-bbc-microbit-wav-sound-file-player-by-cabe)


**BEWARE:** `file_uploader.py` happily trashes all of the remote files
before uplodaing the raw audio file...

## Encoding to raw audio

Manually running `ffmpeg` to produce the raw audio file:

```
$ ffmpeg -y -i microbit.wav -acodec pcm_u8 -f u8 -ac 1 -ar 7912 output.raw
ffmpeg version 4.2.4-1ubuntu0.1 Copyright (c) 2000-2020 the FFmpeg developers
  built with gcc 9 (Ubuntu 9.3.0-10ubuntu2)
  configuration: --prefix=/usr
                 --extra-version=1ubuntu0.1
                 --toolchain=hardened
                 --libdir=/usr/lib/x86_64-linux-gnu
                 --incdir=/usr/include/x86_64-linux-gnu
                 --arch=amd64
                 --enable-gpl
                 --disable-stripping
                 --enable-avresample
                 --disable-filter=resample
                 --enable-avisynth
                 --enable-gnutls
                 --enable-ladspa
                 --enable-libaom
                 --enable-libass
                 --enable-libbluray
                 --enable-libbs2b
                 --enable-libcaca
                 --enable-libcdio
                 --enable-libcodec2
                 --enable-libflite
                 --enable-libfontconfig
                 --enable-libfreetype
                 --enable-libfribidi
                 --enable-libgme
                 --enable-libgsm
                 --enable-libjack
                 --enable-libmp3lame
                 --enable-libmysofa
                 --enable-libopenjpeg
                 --enable-libopenmpt
                 --enable-libopus
                 --enable-libpulse
                 --enable-librsvg
                 --enable-librubberband
                 --enable-libshine
                 --enable-libsnappy
                 --enable-libsoxr
                 --enable-libspeex
                 --enable-libssh
                 --enable-libtheora
                 --enable-libtwolame
                 --enable-libvidstab
                 --enable-libvorbis
                 --enable-libvpx
                 --enable-libwavpack
                 --enable-libwebp
                 --enable-libx265
                 --enable-libxml2
                 --enable-libxvid
                 --enable-libzmq
                 --enable-libzvbi
                 --enable-lv2
                 --enable-omx
                 --enable-openal
                 --enable-opencl
                 --enable-opengl
                 --enable-sdl2
                 --enable-libdc1394
                 --enable-libdrm
                 --enable-libiec61883
                 --enable-nvenc
                 --enable-chromaprint
                 --enable-frei0r
                 --enable-libx264
                 --enable-shared
  WARNING: library configuration mismatch
  avcodec     configuration: --prefix=/usr
                             --extra-version=1ubuntu0.1
                             --toolchain=hardened
                             --libdir=/usr/lib/x86_64-linux-gnu
                             --incdir=/usr/include/x86_64-linux-gnu
                             --arch=amd64
                             --enable-gpl
                             --disable-stripping
                             --enable-avresample
                             --disable-filter=resample
                             --enable-avisynth
                             --enable-gnutls
                             --enable-ladspa
                             --enable-libaom
                             --enable-libass
                             --enable-libbluray
                             --enable-libbs2b
                             --enable-libcaca
                             --enable-libcdio
                             --enable-libcodec2
                             --enable-libflite
                             --enable-libfontconfig
                             --enable-libfreetype
                             --enable-libfribidi
                             --enable-libgme
                             --enable-libgsm
                             --enable-libjack
                             --enable-libmp3lame
                             --enable-libmysofa
                             --enable-libopenjpeg
                             --enable-libopenmpt
                             --enable-libopus
                             --enable-libpulse
                             --enable-librsvg
                             --enable-librubberband
                             --enable-libshine
                             --enable-libsnappy
                             --enable-libsoxr
                             --enable-libspeex
                             --enable-libssh
                             --enable-libtheora
                             --enable-libtwolame
                             --enable-libvidstab
                             --enable-libvorbis
                             --enable-libvpx
                             --enable-libwavpack
                             --enable-libwebp
                             --enable-libx265
                             --enable-libxml2
                             --enable-libxvid
                             --enable-libzmq
                             --enable-libzvbi
                             --enable-lv2
                             --enable-omx
                             --enable-openal
                             --enable-opencl
                             --enable-opengl
                             --enable-sdl2
                             --enable-libdc1394
                             --enable-libdrm
                             --enable-libiec61883
                             --enable-nvenc
                             --enable-chromaprint
                             --enable-frei0r
                             --enable-libx264
                             --enable-shared
                             --enable-version3
                             --disable-doc
                             --disable-programs
                             --enable-libaribb24
                             --enable-liblensfun
                             --enable-libopencore_amrnb
                             --enable-libopencore_amrwb
                             --enable-libtesseract
                             --enable-libvo_amrwbenc
  libavutil      56. 31.100 / 56. 31.100
  libavcodec     58. 54.100 / 58. 54.100
  libavformat    58. 29.100 / 58. 29.100
  libavdevice    58.  8.100 / 58.  8.100
  libavfilter     7. 57.100 /  7. 57.100
  libavresample   4.  0.  0 /  4.  0.  0
  libswscale      5.  5.100 /  5.  5.100
  libswresample   3.  5.100 /  3.  5.100
  libpostproc    55.  5.100 / 55.  5.100
Guessed Channel Layout for Input Stream #0.0 : stereo
Input #0, wav, from 'microbit.wav':
  Duration: 00:00:00.84, bitrate: 1411 kb/s
    Stream #0:0: Audio: pcm_s16le ([1][0][0][0] / 0x0001), \
                        44100 Hz, stereo, s16, 1411 kb/s
Stream mapping:
  Stream #0:0 -> #0:0 (pcm_s16le (native) -> pcm_u8 (native))
Press [q] to stop, [?] for help
Output #0, u8, to 'output.raw':
  Metadata:
    encoder           : Lavf58.29.100
    Stream #0:0: Audio: pcm_u8, 7912 Hz, mono, u8, 63 kb/s
    Metadata:
      encoder         : Lavc58.54.100 pcm_u8
size=       6kB time=00:00:00.83 bitrate=  63.3kbits/s speed=63.3x
video:0kB          \
audio:6kB          \
subtitle:0kB       \
other streams:0kB  \
global headers:0kB \
muxing overhead: 0.000000%
```

A less ridiculous summary of the above (produced by editing it):

```
$ ffmpeg -y -i microbit.wav -acodec pcm_u8 -f u8 -ac 1 -ar 7912 output.raw
ffmpeg version 4.2.4-1ubuntu0.1
Copyright (c) 2000-2020 the FFmpeg developers
  built with gcc 9 (Ubuntu 9.3.0-10ubuntu2)
  configuration: ...
  WARNING: library configuration mismatch
  avcodec     configuration: ...
  libavutil      56. 31.100 / 56. 31.100
  libavcodec     58. 54.100 / 58. 54.100
  libavformat    58. 29.100 / 58. 29.100
  libavdevice    58.  8.100 / 58.  8.100
  libavfilter     7. 57.100 /  7. 57.100
  libavresample   4.  0.  0 /  4.  0.  0
  libswscale      5.  5.100 /  5.  5.100
  libswresample   3.  5.100 /  3.  5.100
  libpostproc    55.  5.100 / 55.  5.100
Guessed Channel Layout for Input Stream #0.0 : stereo
Input #0, wav, from 'microbit.wav':
  Duration: 00:00:00.84, bitrate: 1411 kb/s
    Stream #0:0: Audio: pcm_s16le ([1][0][0][0] / 0x0001), \
                        44100 Hz, stereo, s16, 1411 kb/s
Stream mapping:
  Stream #0:0 -> #0:0 (pcm_s16le (native) -> pcm_u8 (native))
Press [q] to stop, [?] for help
Output #0, u8, to 'output.raw':
  Metadata:
    encoder           : Lavf58.29.100
    Stream #0:0: Audio: pcm_u8, 7912 Hz, mono, u8, 63 kb/s
    Metadata:
      encoder         : Lavc58.54.100 pcm_u8
size=       6kB time=00:00:00.83 bitrate=  63.3kbits/s speed=63.3x
video:0kB          \
audio:6kB          \
subtitle:0kB       \
other streams:0kB  \
global headers:0kB \
muxing overhead: 0.000000%
```

Should I care about the `WARNING: library configuration mismatch`
notification? Can I cure it?

## Playing back the raw audio

**BEWARE!** Playback of the raw audio through headphones could deafen
you for life and may have deafened me.

`play` is a `sox` command. The raw audio file has been stripped of all its
metadata, and now needs to be supplied with it in order to play back. Based
on what the encoding used, I was able to ferret out:

* `-b 8`      = 8-bit sample values (0 to 255) OR (-128 to 127)
* `-e signed` = the later: (-128 to 127)
* `-r 7912`   = sample rate 7912 Hz
* `-c 1`      = 1 channel, i.e. mono
* `-v 0.1`    = play back at 10% of the original (?) volume

Putting it all together:

```
    $ play -b 8 -e signed -r 7912 -c 1 -v 0.1 output.raw
    play WARN alsa: can't encode 0-bit Unknown or not applicable
    play WARN alsa: can't encode 8-bit Signed Integer PCM

    output.raw:

     File Size: 6.61k     Bit Rate: 63.3k
      Encoding: Signed PCM
      Channels: 1 @ 8-bit
    Samplerate: 7912Hz
    Replaygain: off
      Duration: 00:00:00.84

    In:100%  00:00:00.84 [00:00:00.00] Out:6.61k [    -=|=-    ]   Clip:0
    Done.
```

Since it played back in a semi-intelligible fashion, though rather
staticky, I'm not sure what the warnings from ALSA are talking about.

My bad: In `pcm_u8` the `u` is for **unsigned**. Changing the encoding
from `signed` to `unsigned` not only softened the sound but also
cleaned it up immensely. It's still clearly lossy, but recognizable.


```
    $ play -b 8 -e unsigned -r 7912 -c 1 -v 0.2 output.raw
    play WARN alsa: can't encode 0-bit Unknown or not applicable

    output.raw:

     File Size: 6.61k     Bit Rate: 63.3k
      Encoding: Unsigned PCM
      Channels: 1 @ 8-bit
    Samplerate: 7912Hz
    Replaygain: off
      Duration: 00:00:00.84

    In:100%  00:00:00.84 [00:00:00.00] Out:6.61k [     =|=     ]   Clip:0
    Done.
```

----

The results of attempting to play the above back through the micro:bit
were **very** disappointing, to the point where I really cannot be
certain that the sound is anything like the original. So, let's try
something simpler: A nice 1-second "concert A" sine wave at 440 Hz.

```
$ sox -n sine.wav synth 1.0 sine 440.0
$ ffmpeg -y -i sine.wav -acodec pcm_u8 -f u8 -ac 1 -ar 7912 output.raw
$ play -b 8 -e unsigned -r 7912 -c 1 -v 0.2 output.raw
$ python3
>>> import microfs
>>> ubits_list = microfs.get_serial()
>>> ubits_list
>>> microfs.ls()
>>> microfs.put("output.raw", target=None, serial=None)
>>> microfs.ls()
```

And it still sounds like garbage on the micro:bit. So, clearly I'm not
understanding.

OK... So... According to the documentation, the sample rate is
7**8**12.5 samples per second... and all the above code I've
downloaded uses 7**9**12... Whoopsie...

----
