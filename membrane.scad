// membrane.scad
// Written by Kevin Cole <ubuntourist@hacdc.org> 2021.03.21
//
// A simulation of  diaphragm / membrane
// e.g. an eardrum, speaker cone, or microphone.
//

use <stdlib.scad>   // import my "homegrown" torus module

PI = 180.0;                 // Pi in degrees
thickness = 2.0;            // membrane fabric thickness
major     = 100.0;          // radius "hole"
minor     = 5.0;            // radius of "tube"
fabric    = major - minor;  // radius of fabric
amp       = 0.10;           // Amplitude (0.0 to 1.0)
cable     = 300.0;          // cable length
wires     = major / 4;      // radius of cable

// "Dormant" speaker membrane and mounting "ring"
rotate([90, 0, 0])
//  union() {
    torus(major, minor);                     // ring
//  color("Gray")                            // membrane
//    cylinder(thickness, fabric, fabric, $fn=200);
//  }

// "Ballooning" speaker membrane
direction =  $t < 0.5 ? $t : 1 - $t;
stretch = (fabric * 2) - (sin(direction * PI * amp) * fabric);
difference() {
  difference() {
    displacement = sqrt(pow(stretch, 2) - pow(fabric, 2));
    translate([0, displacement, 0])
      union() {
        difference() {
          color("Gray")
            sphere(stretch, $fn=200);              // "front"
          color("Gray")
            sphere(stretch - thickness, $fn=200);  // "back"
        }
        translate([0, thickness+6-stretch, 0])
          rotate([90, 0, 0])
            cylinder(thickness, major/4, major/4, $fn=200);
        translate([0, thickness-stretch, 0])
          rotate([90, 0, 0])
            color("Red", 0.2)
              cylinder(cable, wires, wires, $fn=200);
        }

      translate([-2 * major, 0, -2 * major])
        cube(4 * major);                         // "mask"
  }
  translate([-100, -500, -100])
    cube([200, 200, 200]);                       // "mask"
}
//lightning = rands()
//translate([0, -100.0, 0])
//  rotate([90, 0, 0])
//    cylinder(100.0, 25.0, 25.0, $fn=200);