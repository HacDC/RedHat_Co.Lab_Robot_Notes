# Twelfth Root of Two
frequency = 440
# 2.0 ** (1.0 / 12.0)
twelfth_root = 1.0594630943592953
for index in range(12):
    basic.pause(200)
    music.play_tone(frequency, music.beat(BeatFraction.WHOLE))
    frequency = frequency * twelfth_root
    print(frequency)

